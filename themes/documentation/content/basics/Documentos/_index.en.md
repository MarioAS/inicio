
## Inicio

Esta es la documentación de [GitLab Pages](https://about.gitlab.com/product/pages/) con [HUGO](https://gohugo.io/), La documentación es exclusivamente de crear nuestra página en [Gitlab.com](gitlab.com) desde cero, para empezar debemos obviamente crearnos una cuenta, una vez creada nuestra cuenta, vamos a crear nuestro primer repositorio, en este caso y por razones lógicas debe ser un repositorio público, por que al ser una página con documentación y se quiere compartir debe ser pública.


## Usuarios y grupos de Gitlab

Tenemos la posibilidad de que nuestro proyecto este ubicado en una instancia de sólo usuario o instancia grupo 
To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.


## Creando y afinando GitLab CI/CD

Primero vamos a entender que es CI/CD (Continuos Integration/Continuos Delivery/Continuos Deployment), es una herramienta dentro de GitLab para el desarrollo de software a través de las metodologías continuas, CI/CD está configurado por un archivo llamado `.gitlab-ci.yml` ubicado en el la raíz del repositorio,  las secuencias de comandos son ejecutadas por `Gitlab Runner`, Gitlab Runner es un proyecto open source que es usado para correr tareas y enviar los resultados a GitLab, es usado conjuntamente con `Gitlab CI`

Tomemos en cuenta que cada que creamos un repositorio por medio de un template definido por GitLab y como es nuestro caso el SSG (Site Static Generator)HUGO, creará por defecto el archivo `.gitlab-ci.yml`, es caso de hacer uso de un template necesitamos crearlo, y lo más recomendado es hacerlo desde la UI (Interfaz) de Gitlab